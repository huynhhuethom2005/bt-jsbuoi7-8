var inputNumber = document.getElementById('inputNumber');
var valueArray = []

function addValueArray() {
    valueArray.push(inputNumber.value)
    document.getElementById('valueArray').innerHTML = `Mảng: ${valueArray}`
}

function tongDuong() {
    let result = 0
    valueArray.forEach(value => {
        if (value >= 0) {
            result += value * 1
        }
    })
    document.getElementById('tongDuong').innerHTML = `Tổng các số dương là: ${result}`
}

function demSoDuong() {
    let result = 0
    valueArray.forEach(value => {
        if (value >= 0) {
            result += 1
        }
    })
    document.getElementById('demSoDuong').innerHTML = `Có ${result} số dương`
}

function findMin() {
    document.getElementById('findMin').innerHTML = `Số nhỏ nhất là ${Math.min(...valueArray)}`
}

function findIntMin() {
    let intArray = []
    valueArray.forEach(value => {
        if (value >= 0) {
            intArray.push(value)
        }
    })
    document.getElementById('findIntMin').innerHTML = `Số dương nhỏ nhất là ${Math.min(...intArray)}`
}

function lastSoChan() {
    let soChan = -1
    valueArray.forEach(value => {
        if (value % 2 == 0) {
            soChan = value
        }
    })
    document.getElementById('lastSoChan').innerHTML = `Số chẵn cuối cùng là ${soChan}`
}

function changeIndex() {
    let viTri1 = document.getElementById('viTri1').value * 1
    let viTri2 = document.getElementById('viTri2').value * 1
    var valueExtra

    valueExtra = valueArray[viTri1]
    valueArray[viTri1] = valueArray[viTri2]
    valueArray[viTri2] = valueExtra

    document.getElementById('changeIndex').innerHTML = `Mảng: ${valueArray}`
}

function sortArray() {
    valueArray.sort(function (a, b) { return a - b });
    document.getElementById('sortArray').innerHTML = `Mảng: ${valueArray}`
}

// function findIsprime() {
//     valueArray.forEach((value) => {
//         if (value > 2) {
//             if (value % 1 == 0 && value %  == 0) {
//                 return  document.getElementById('findIsprime').innerHTML = value
//             }
//         } else {
//             return  document.getElementById('findIsprime').innerHTML = -1
//         }
//     })
// }

function findInt() {
    let result = 0
    valueArray.forEach((value) => {
        if (Number.isInteger(value)) {
            result += 1
        }
    })
    document.getElementById('findInt').innerHTML = `Có ${result} số nguyên`
}

function compare() {
    let soAm = 0
    let soDuong = 0
    valueArray.forEach((value) => {
        if (value > 0) {
            soDuong += 1
        } else if (value < 0) {
            soAm += 1
        }
    })

    if (soDuong > soAm) {
        return document.getElementById('compare').innerHTML = `Số dương nhiều hơn`
    } else if (soDuong < soAm) {
        return document.getElementById('compare').innerHTML = `Số âm nhiều hơn`
    }
}
